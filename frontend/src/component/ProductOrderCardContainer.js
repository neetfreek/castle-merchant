/******************************************************************
* ProductCardOrderContainer component: Parent of ordered product  *
*   card components                                               *
*******************************************************************
* - Ordered products' information stored in state object as list  *
* - Orders are mapped to ProductOrderCard components              *
*******************************************************************/
import React from 'react';
import ProductOrderCard from './ProductOrderCard';

export default class ProductOrderCardContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productOrderCards: []
    };
  }

  render() {
    return (
      <div className="row mt-5">
        {this.state.productOrderCards.map(order => <ProductOrderCard key={order.id} {...order} />)}
      </div>
    )
  };
}
/******************************************************************
* FormSignin component: user sign in form                         *
*******************************************************************
* - handleChange() called on user data input into form            *
* - handleSubmit() called on HTML form sumbission                 *
* - handleNewUser() passed from ModalWindowSignIn component as    *
*     prop, called on "new user" click, sets parent handleNewUser *
*     state to true, rendering "new user" form instead of "sign   *
*     in"                                                         *
*******************************************************************/
import { FORM_SIGN_IN_REGISTRATION } from '../constants';

import React from 'react';

export default class FormSignIn extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      errorMessage: ""
    };
  }

  handleInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    });
  }

  // TODO: Implement after backend API developed
  handleSubmit(event) {
    event.preventDefault();
    console.log(JSON.stringify(this.state));
  }

  render() {
    // Error message
    let errrorMessage = null;
    if (this.state.errorMessage.length !== 0) {
      errrorMessage = <h5 className="mb-4 text-danger">{this.state.errormessage}</h5>;
    }
    return (
      <div>
        {errrorMessage}
        <form onSubmit={this.handleSubmit}>
          <h5 className="mb-4">{FORM_SIGN_IN_REGISTRATION.SIGN_IN}</h5>
          <div className="form-group">
            <label htmlFor="email">{FORM_SIGN_IN_REGISTRATION.EMAIL}</label>
            <input name="email" type="email" className="form-control" id="email"
            placeholder={FORM_SIGN_IN_REGISTRATION.PLACEHOLDER_EMAIL} onChange={this.handleInputChange} />
          </div>
          <div className="form-group">
            <label htmlFor="pass">{FORM_SIGN_IN_REGISTRATION.PASSWORD}</label>
            <input name="password" type="password" className="form-control" id="pass" onChange={this.handleInputChange} />
          </div>
          <div className="form-row text-center">
            <div className="col-12 mt-2">
              <button type="submit" className="btn btn-success btn-large">{FORM_SIGN_IN_REGISTRATION.SIGN_IN}</button>
            </div>
            <div className="col-12 mt-2">
              <button type="submit" className="btn btn-link text-info"
                onClick={() => this.props.handleNewUser()}>{FORM_SIGN_IN_REGISTRATION.NEW_USER}</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
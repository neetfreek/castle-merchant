/******************************************************************
* ProductCardOrder component: Display ordered product             *
*******************************************************************
* Ordered product information passed from parent as props         *
*******************************************************************/
import { ORDER } from '../constants';

import React from 'react';

export default class ProductOrderCard extends React.Component {
  render() {
    return (
      <div className="col-12">
        <div className="card text-center">
          <div className="card-header">
            <h5>{this.props.productname}</h5>
          </div>
          <div className="card-body">
            <div className="row">
              <img src={this.props.img} alt={this.props.imgalt}
                className="img-thumbnail float-left"></img>
            </div>
            <div className="col-6">
              <p className="card-text">{this.props.desc}</p>
              <div className="mt-4">
                {ORDER.PRICE} <strong>{this.props.price}</strong>
              </div>
            </div>
          </div>
          <div className="card-footer text-muted">
            {ORDER.PURCHASED} {this.props.days} {ORDER.DAYS_AGO}
          </div>
        </div>
        <div className="mt-3" />
      </div>
    )
  };
}
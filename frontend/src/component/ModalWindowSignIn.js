/******************************************************************
* ModalWindowSignIn component: parent of FormSignIn and           *
*   FormRegistration components                                   *
*******************************************************************
* - Render "sign in" or "new user" form based on                  *
*     showFormRegistration state                                  *
* - pass handleNewUser as prop to FormSignIn if render "sign in"  *
*     form                                                        *
*******************************************************************/
import FormSignIn from './FormSignIn';
import FormRegistration from './FormRegistration';

import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { MODAL_WINDOW_SIGN_IN } from '../constants';

export default class ModalWindowSignIn extends React.Component {
  constructor(props) {
    super(props);
    this.handleNewUser = this.handleNewUser.bind(this);
    this.state = {
      showFormRegistration: false
    };
  }

  handleNewUser() {
    this.setState({
      showFormRegistration: true
    });
  }

  render() {
    let modalBody = <FormSignIn handleNewUser={this.handleNewUser} />
    if (this.state.showFormRegistration === true) {
      modalBody = <FormRegistration />
    }
    return (
      <Modal id="signin" tabIndex="-1" role="dialog" isOpen={this.props.showModal} toggle={this.props.toggle}>
      <div role="document">
        <ModalHeader toggle={this.props.toggle} className="bg-success text-white">
          {MODAL_WINDOW_SIGN_IN.SIGN_IN}
          {/* TODO: implement after nav bar
          <button className="close">
            <span aria-hidden="true">&times;</span>
          </button> */}
        </ModalHeader>
        <ModalBody>
          {modalBody}
        </ModalBody>
      </div>
      </Modal>
    );
  }
}
/******************************************************************
* FormRegistration component: user registration form              *
*******************************************************************
* - handleChange() called on user data input into form            *
* - handleSubmit() called on HTML form sumbission                 *
*******************************************************************/
import { FORM_SIGN_IN_REGISTRATION } from '../constants';

import React from 'react';

export default class FormRegistration extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      errorMessage: ""
    };
  }

  handleInputChange(event) {
    event.preventDefault();
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    });
  }

  // TODO: Implement after backend API developed
  handleSubmit(event) {
    event.preventDefault();
    console.log(this.state);
  }

  render() {
    // Error message
    let errrorMessage = null;
    if (this.state.errorMessage.length !== 0) {
      errrorMessage = <h5 className="mb-4 text-danger">{this.state.errormessage}</h5>;
    }
    return (
      <div>
        {errrorMessage}
        <form onSubmit={this.handleSubmit}>
          <h5 className="mb-4">{FORM_SIGN_IN_REGISTRATION.REGISTRATION}</h5>
          <div className="form-group">
            <label htmlFor="username">{FORM_SIGN_IN_REGISTRATION.USER_NAME}</label>
            <input name="username" type="text" className="form-control" id="username" onChange={this.handleInputChange}
            placeholder={FORM_SIGN_IN_REGISTRATION.PLACEHOLDER_NAME} />
          </div>
          <div className="form-group">
            <label htmlFor="email">{FORM_SIGN_IN_REGISTRATION.EMAIL}</label>
            <input name="email" type="email" className="form-control" id="email" onChange={this.handleInputChange}
            placeholder={FORM_SIGN_IN_REGISTRATION.PLACEHOLDER_EMAIL} />
          </div>
          <div className="form-group">
            <label htmlFor="pass">{FORM_SIGN_IN_REGISTRATION.PASSWORD}</label>
            <input name="password1" type="password" className="form-control" id="pass1" onChange={this.handleInputChange} />
          </div>
          <div className="form-group">
            <label htmlFor="pass">{FORM_SIGN_IN_REGISTRATION.PASSWORD_CONFIRM}</label>
            <input name="password2" type="password" className="form-control" id="pass2" onChange={this.handleInputChange} />
          </div>
          <div className="form-row text-center">
            <div className="col-12 mt-2">
              <button type="submit" className="btn btn-success btn-large">{FORM_SIGN_IN_REGISTRATION.SIGN_IN}</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
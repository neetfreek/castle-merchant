/******************************************************************
* Modal Window components                                         *
*******************************************************************
* Includes:                                                       *
*   Buy Item modal window                                         *
* toggle prop callback function used to toggle isOpen             *
* isOpen prop true display modal, false hide                      *
*******************************************************************/
import { MODAL_WINDOW_BUY } from '../constants';
import CardInformation from './CardInformation';

import React from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'

export default class ModalWindowBuy extends React.Component {
  render() {
    return (
      <Modal id="buy" tabIndex="-1" role="dialog" isOpen={this.props.showModal} toggle={this.props.toggle}>
        <div role="document">
          <ModalHeader toggle={this.props.toggle} className="bg-success text-white">
            {MODAL_WINDOW_BUY.BUY}
          </ModalHeader>
          <ModalBody>
            <CardInformation show={true} operation={MODAL_WINDOW_BUY.PAY} toggle={this.props.toggle} />
          </ModalBody>
        </div>
      </Modal>
    );
  }
}
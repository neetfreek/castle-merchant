/******************************************************************
* Card Container component: contains product cards                *
*******************************************************************
* fetch() used in production to fetch an API address for cards'   *
*   data                                                          *
* * componentDidMount overridden, using location prop to show:    *
*   - Product data from cards.json when showing main products     *
*       page                                                      *
*   - Product data from promos.json when showing promos page      *
* Map cards' data to cardItems collection, passing properties     *
*   with ... where names in data match card's props               *
*******************************************************************/
import ProductCard from './ProductCard.js'
import { PRODUCT_CARD_CONTAINER } from '../constants.js';

import React from 'react'

export default class ProductCardContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productCards: []
    };
  }

  componentDidMount() {
    fetch(this.props.location)
      .then(res => res.json())
      .then((result) => {
        this.setState({
          productCards: result
        });
      });
  }

  render() {
    const productCards = this.state.productCards;
    const productCardItems = productCards.map(
      productCard => <ProductCard
        key={productCard.id} promo={this.props.promo}
        {...productCard}
        showModalWindowBuy={this.props.showModalWindowBuy} />
    )
    return (
      <div className="container pt-4">
        <h3 className='text-center text-primary'>{PRODUCT_CARD_CONTAINER.PRODUCTS}</h3>
        <div className="pt-4 row">
          {productCardItems}
        </div>
      </div>
    )
  };
}

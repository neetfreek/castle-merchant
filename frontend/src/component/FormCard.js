/******************************************************************
* FormCard component: customers' credit cards                     *
*******************************************************************
* - Card processing states change based on success/failure of     *
*       transaction                                               *
* - render() calls render... functions based on card processing   *
*       state (status) from token outcome to return different     *
*       bodies                                                    *
* - handleSubmit() called on credit card HTML form submission;    *
*     - Creates, retrieves token representing card entered        *
*           using Stripe API which is used to charge card in      *
*           backend                                               *
*******************************************************************/
import { FORM_CARD } from '../constants.js';

import React from 'react';
import { CardElement } from 'react-stripe-elements';

// Hosts credit card form
export default class FormCard extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      status: FORM_CARD.STATE_INITIAL,
    }
  }
  // Default body
  renderCreditCardInformation() {
    const cardElementStyle = {
      base: {
        "fontSize": "20px",
        "color": "#495057",
        "fontFamily": 'apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif'
      }
    }

    const usersavedcard =
      <div>
        <div className="form-row text-center">
          <button type="button" className="btn btn-outline-success text-center mx-auto">
            {FORM_CARD.USE_SAVED_CARD}
          </button>
        </div>
        <hr />
      </div>

    const remembercardcheck =
      <div className="form-row form-check text-center">
        <input className="form-check-input" type="checkbox" value="" id="remembercardcheck" />
        <label className="form-check-label" htmlFor="remembercardcheck">
          {FORM_CARD.REMEMBER_CARD}
        </label>
      </div>;

    return (
      <div>
        {usersavedcard}
        <h5 className="mb-4">{FORM_CARD.INFO_PAYMENT}</h5>
        <form onSubmit={this.handleSubmit}>
          <div className="form-row">
            <div className="col-lg-12 form-group">
              <label htmlFor="cc-name">
                {FORM_CARD.NAME_ON_CARD}
              </label>
              <input id="cc-name" name='cc-name' className="form-control"
                placeholder={FORM_CARD.NAME_ON_CARD} onChange={this.handleInputChange} type='text' />
            </div>
          </div>
          <div className="form-row">
            <div className="col-lg-12 form-group">
              <label htmlFor="card">{FORM_CARD.INFO_CARD}</label>
              <CardElement id="card" className="form-control" style={cardElementStyle} />
            </div>
          </div>
          {remembercardcheck}
          <hr className="mb-4" />
          <button type="submit" className="btn btn-success btn-large" >{this.props.operation}</button>
        </form>
      </div>
    );
  }

  handleInputChange(event) {
    this.setState({
      value: event.target.value
    });
  };

  async handleSubmit(event) {
    event.preventDefault();
    console.log("handleSubmit with ", this.state.value);

    let { token } = await this.props.stripe.createToken({ name: this.state.value });
    if (token == null) {
      console.log("Invalid token");
      this.setState({
        status: FORM_CARD.STATE_FAILED
      });
      return;
    }

    let response = await fetch("/charge", {
      method: "POST",
      headers: { "Content-Type": "text/plain" },
      body: JSON.stringify({
        token: token.id,
        operation: this.props.operation,
      })
    });
    console.log("response.ok: ", response.ok);
    if (response.ok) {
      console.log("Purchase successful");
      this.setState({ status: FORM_CARD.STATE_SUCCESS });
    }
  }

  // Success body
  renderSuccess() {
    return (
      <div>
        <h5 className="mb-4 text-success">{FORM_CARD.SUCCESS}</h5>
        <button type="submit" className="btn btn-success btn-large"
          onClick={() => { this.props.toggle() }}>{FORM_CARD.OK}</button>
      </div>
    );
  }

  // Failure body
  renderFailure() {
    return (
      <div>
        <h5 className="mb-4 text-danger"> {FORM_CARD.FAILURE}</h5>
        {this.renderCreditCardInformation()}
      </div>
    );
  }

  // Set body as return value of appropriate render... function
  render() {
    let body = null;
    switch (this.state.status) {
      case FORM_CARD.STATE_SUCCESS:
        body = this.renderSuccess()
        break;
      case FORM_CARD.STATE_FAILED:
        body = this.renderFailure()
        break;
      default:
        body = this.renderCreditCardInformation();
    }
    return (
      <div>
        {body}
      </div>
    );
  }
}
/******************************************************************
* ProductCard component: display product information to user       *
*******************************************************************
* Includes:                                                       *
*   - Image                                                       *
*   - Alt text                                                    *
*   - Title                                                       *
*   - Price, colour, text dependent on whether product is on      *
*       promotion                                                 *
*   - Description                                                 *
*   - Buy button, calls showBuyModal() on click                   *
*******************************************************************/
import { PRODUCT_CARD } from '../constants';

import React from 'react'

export default class ProductCard extends React.Component {
  render() {
    const priceColour = (this.props.promo) ? "text-danger" : "text-dark"
    const sellPrice = (this.props.promo) ? this.props.promotion : this.props.price
    return (
      <div className="col-md-6 col-lg-4 d-flex align-items-stretch">
        <div className="card mb-3">
          <img className="card-img-top" src={this.props.img} alt={this.props.imgalt} />
          <div className="card-body">
            <h4 className="card-title">{this.props.productname}</h4>
            {PRODUCT_CARD.PRICE} <strong className={priceColour}>{sellPrice}</strong>
            <p className="card-title">{this.props.desc}</p>
            <button
              className="btn btn-success text-white"
              onClick={() => { this.props.showModalWindowBuy(this.props.id, sellPrice) }}>
              {PRODUCT_CARD.BUY}</button>
          </div>
        </div>
      </div>
    );
  };
}
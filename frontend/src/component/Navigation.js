/******************************************************************
* Navigation menu component                                       *
*******************************************************************
* - NavBar displays "sign in" or username based on user logged in *
* - User information passed from parent component as prop         *
* - NavLinks routed:                                              *
*     / -> ProductCardContainer component, ProductCard component  *
*       children mapped to products                               *
*     /promos -> ProductCardContainer component, ProductCard      *
*       component children mapped to promotional products         *
*     /orders -> ProductOrderCardContainer component,             *
*        ProductOrderCard children components mapped to ordered   *
*         products                                                *
*     /about -> About component                                   *
* - NavLink components used to create links to navigate to other  *
*   components                                                    *
* - NavLink components must be contained in a Router              *
*******************************************************************/
import { NAVIGATION, ROUTE } from '../constants';

import React from 'react'
import { NavLink } from 'react-router-dom'

export default class Navigation extends React.Component {
  showLoggedIn() {
    return (
      <div className="navbar-brand order-1 text-white my-auto">
        <div className="btn-group">
          <button type="button" className="btn btn-success dropdown-toggle"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {this.props.user.name}
          </button>
          <div className="dropdown-menu">
            <button className="btn dropdown-item">
              {NAVIGATION.SIGN_OUT}
            </button>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-success fixed-top">
          <div className="container">
            {
              this.props.user.loggedin ?
                this.showLoggedIn()
                : <button type="button" className="navbar-brand order-1 btn btn-success"
                  onClick={() => { this.props.showModalWindowSignIn(); }}>
                  {NAVIGATION.SIGN_IN}
                </button>
            }
            <div className="navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <NavLink className="nav-item nav-link" to={ROUTE.HOME}>{NAVIGATION.HOME}</NavLink>
                <NavLink className="nav-item nav-link" to={ROUTE.PROMOS}>{NAVIGATION.PROMOTIONS}</NavLink>
                {
                  this.props.user.loggedin ?
                    <NavLink className="nav-item nav-link" to={ROUTE.ORDERS}>{NAVIGATION.ORDERS}</NavLink>
                    : null
                }
                <NavLink className="nav-item nav-link" to={ROUTE.ABOUT}>{NAVIGATION.ABOUT}</NavLink>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
/******************************************************************
* CardInformation component: Host CardForm, inject Stripe API and *
*   pass Stripe API key                                           *
*******************************************************************
* - Uses Stripe test API key                                      *
* - Component used as a child in buy ModalWindow component        *
*******************************************************************/

import FormCard from './FormCard';

import React from 'react';
import { injectStripe, StripeProvider, Elements } from 'react-stripe-elements';

export default function CreditCardInformation(props) {
  if (!props.show) {
    return <div />;
  }
  const CCFormWithStripe = injectStripe(FormCard);
  return (
    <div>
      <StripeProvider apiKey="pk_test_LwL4RUtinpP3PXzYirX2jNfR">
        <Elements>
          <CCFormWithStripe operation={props.operation} />
        </Elements>
      </StripeProvider>
    </div>
  );
}
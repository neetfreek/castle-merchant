/******************************************************************
* About page component                                            *
*******************************************************************
* Compoent written as functional component instead of class owing *
*   to its simplicity.                                            *
******************************************************************/
import { ABOUT } from '../constants';

import React from 'react'

// previously a functional component
export default class About extends React.Component {
  render() {
    return (
      <div className="row mt-5">
        <div className="col-12 order-lg-1">
          <h3 className="mb-4">{ABOUT.HEADING}</h3>
          <p>{ABOUT.INTRO}</p>
          <p>{ABOUT.BODY}</p>
          <p>{ABOUT.CONCLUSION}</p>
        </div>
      </div>
    );
  }
}
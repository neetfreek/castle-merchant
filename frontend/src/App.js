/******************************************************************
* App component serves as the entry point for all components      *
*******************************************************************
* - Contains global information in state object, passed as props  *
*     to children as necessary                                    *
* - showModalWindow... functions display ModalWindow...           *
*     components, through state changes, hidden by                *
      toggleModalWindow... by inverting states                    *
* - Router component contains all NavLink instances (from         *
*     Navigation component)                                       *
* -  Route component links URL paths (corresponding to NavLink    *
*     paths) to respective components                             *
*******************************************************************/
import { ROUTE } from './constants';

import Navigation from './component/Navigation';
import ProductCardContainer from './component/ProductCardContainer';
import ProductOrderCardContainer from './component/ProductOrderCardContainer';
import About from './component/About';
import ModalWindowBuy from './component/ModalWindowBuy';
import ModalWindowSignIn from './component/ModalWindowSignIn';

import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

export default class App extends React.Component {
  // State will be populated from backend
  constructor(props) {
    super(props);
    this.showModalWindowBuy = this.showModalWindowBuy.bind(this);
    this.showModalWindowSignIn = this.showModalWindowSignIn.bind(this);
    this.toggleModalWindowBuy = this.toggleModalWindowBuy.bind(this);
    this.toggleModalWindowSignIn = this.toggleModalWindowSignIn.bind(this);
    this.state = {
      user: {
        loggedin: false,
        name: ""
      }
    }
  }

  showModalWindowBuy() {
    // const state = this.state;
    // const newState = Object.assign({}, state, { showModalWindowBuy: true, id: id, price: price });
    // this.setState(newState);
  }

  showModalWindowSignIn() {
    const state = this.state;
    const newState = Object.assign({}, state, { showSignInModal: true });
    this.setState(newState);
  }

  toggleModalWindowBuy() {
    const state = this.state;
    const newState = Object.assign({}, state, { showSignInModal: !state.showSignInModal });
    this.setState(newState);
  }

  toggleModalWindowSignIn() {
    const state = this.state;
    const newState = Object.assign({}, state, { showBuyModal: !state.showBuyModal });
    this.setState(newState);
  }

  render() {
    return (
      <div>
        <Router>
          <div>
            <Navigation user={this.state.user} showModalWindowSignIn={this.showModalWindowSignIn} />
            <div className="container pt-4 mt-4">
              <Route exact path={ROUTE.HOME}
                render={() => <ProductCardContainer
                  location='productCards.json' showModalWindowBuy={this.showModalWindowBuy} />} />
              <Route path={ROUTE.PROMOS}
                render={() => <ProductCardContainer
                  location='productCardsPromo.json' promo={true} showModalWindowBuy={this.showModalWindowBuy} />} />
              {
                this.state.user.loggedin ?
                  <Route path={ROUTE.ORDERS}
                    render={() => <ProductOrderCardContainer location='user.json' />} />
                  : null
              }
              <Route path={ROUTE.ABOUT} component={About} />
            </div>
            <ModalWindowBuy showModal={this.state.showModalWindowBuy}
              toggle={this.toggleModalWindowBuy} />
            <ModalWindowSignIn showModal={this.state.showModalWindowSignIn}
              toggle={this.toggleModalWindowSignIn} />
          </div>
        </Router>
      </div>
    )
  };
}
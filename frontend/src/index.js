/******************************************************************
* Communicates components to be rendered on DOM                   *
*******************************************************************
* Sends to reactDOM to display components in entry point root div *
*   element in index.html                                         *
*******************************************************************/
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { register } from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
register();

export const ABOUT = {
  HEADING: "About Castle Merchant",
  INTRO: "Castle Merchant is an online store for the castle connesurier",
  BODY: "We have specialsed in trading castles since our first customer, King Henry V of England.",
  CONCLUSION: "Our store is much modernised today, but our respect for history and fine, palatial homes persists.",
}

export const FORM_CARD = {
  FAILURE: "Invalid card information, try again",
  INFO_PAYMENT: "Payment Info",
  INFO_CARD: "Card Information",
  NAME_ON_CARD: "Name on card:",
  OK: "OK",
  REMEMBER_CARD: "Remember card?",
  STATE_INITIAL: "INITIAL",
  STATE_SUCCESS: "SUCCESS",
  STATE_FAILED: "FAILED",
  SUCCESS: "Request successful",
  USE_SAVED_CARD: "Use saved card?",
};

export const FORM_SIGN_IN_REGISTRATION = {
  EMAIL: "Email:",
  INFO: "User details",
  SIGN_IN: "Existing User Sign In",
  NEW_USER: "New user? Register",
  PASSWORD: "Password:",
  PASSWORD_CONFIRM: "Confirm password:",
  PLACEHOLDER_NAME: "Your Name",
  PLACEHOLDER_EMAIL: "example@domain.com",
  REGISTRATION: "New User Registration",
  USER_NAME: "User name:"

}

export const MODAL_WINDOW_BUY = {
  BUY: "Buy Item",
  PAY: "Pay"
}

export const MODAL_WINDOW_SIGN_IN = {
  SIGN_IN: "Sign In"
}

export const NAVIGATION = {
  ABOUT: "About",
  HOME: "Home",
  ORDERS: "Orders",
  PROMOTIONS: "Promotions",
  SIGN_IN: "Sign in",
  SIGN_OUT: "Sign out"
}

export const ORDER = {
  PRICE: "Price:",
  PURCHASED: "Purchased",
  DAYS_AGO: "days ago"
}

export const PRODUCT_CARD = {
  PRICE: "Price:",
  BUY: "Buy"
}

export const PRODUCT_CARD_CONTAINER = {
  PRODUCTS: "Products"
}

export const ROUTE = {
  ABOUT: "/about",
  HOME: "/",
  ORDERS: "/orders",
  PROMOS: "/promos"
}
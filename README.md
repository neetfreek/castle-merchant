# Castle Merchant

Castle Merchant is a web store application where users can browse and buy castles. Users log in to their accounts to manage their purchases.

## Overview

The web store consists of:

- Three main pages which include a navigation bar and sign in button:
  - Home
  - Promotions
  - About
- A page containing different castles for sale
- A user page

## Payment Processing

The application uses [Stripe](https://stripe.com) to process payments. The integration is as follows:

- `index.html`
  - Include Stripe.js by adding script tag to `head` of `index.html`
- `FormCard.js`
  - A Stripe [CardElement](https://github.com/stripe/react-stripe-elements#component-reference) component is rendered in the FormCard component which tokenises information, translates placeholders, validate use input
- `CardInformation.js`
  - A [`stripeProvider`](https://github.com/stripe/react-stripe-elements#injectstripe-hoc) context is rendered in the FormCard component which accepts an `apiKey`
  - The FormCard component is wrapped with [`injectStripe`](https://github.com/stripe/react-stripe-elements#injectstripe-hoc) to communicate with `Stripe.js` to create sources, tokens. This is childed to an `Elements` Element component

## Tech Stack

This is a fullstack application, composed of a React component-based frontend connected to a Go backend and MySQL database.

### Frontend

The application's frontend is written in a combination of [JavaScript](https://www.javascript.com/) and [JSX](https://jsx.github.io/). It uses the [React](https://reactjs.org/) interactive UI library, and was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Declarative routing is handled using [react-router-dom](https://www.npmjs.com/package/react-router-dom), and modal windows are written using the [reactstrap](https://reactstrap.github.io/) package.

This application uses the [Boostrap](https://getbootstrap.com/) frontend component library's BootstrapCDN.

### Backend

The application's backend is written in [Go](https://golang.org/). The [Gin](https://github.com/gin-gonic/gin) HTTP webframework is used for the creation of RESTful web APIs and HTTP request route grouping. The [GORM](https://gorm.io/) ORM library is used to connect the web API backend to a [MySQL](https://www.mysql.com/) database.

## Images

The images used in this project are from [pexels](https://www.pexels.com/), an online serve providing free stock photos created by independent contributors.
